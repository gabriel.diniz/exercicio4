import java.util.Scanner;

public class exercicio4 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int cod1, valor1, quantidade1, cod2, valor2, quantidade2, ipi, valorTotal;

        System.out.println("Insira a porcentagem do IPI: ");
        ipi = in.nextInt();

        System.out.println("Insira o código da peça 1: ");
        cod1 = in.nextInt();

        System.out.println("Insira o valor da peça 1: ");
        valor1 = in.nextInt();

        System.out.println("Insira a quantidade: ");
        quantidade1 = in.nextInt();

        System.out.println("Insira o código da peça 2: ");
        cod2 = in.nextInt();

        System.out.println("Insira o valor da peça 2: ");
        valor2 = in.nextInt();

        System.out.println("Insira a quantidade: ");
        quantidade2 = in.nextInt();

        valorTotal = (valor1 * quantidade1) + (valor2 * quantidade2);
        valorTotal +=  (valorTotal * ipi) / 100;

        System.out.println("O valor da peça 1 é: " +(valor1*quantidade1));

        System.out.println("O valor da peça 2 é: " +(valor2*quantidade2));

        System.out.println("O valor total com IPI: "+ valorTotal);

    }

}
